package utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class GetConfig {

    private String botToken, ownerID;

    public void GetConfig(){
        List<String> config = null;
        try {
            config = Files.readAllLines(Paths.get("config.txt"), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        botToken = config.get(0);
        ownerID = config.get(1);
    }

    public String getBotToken() {
        return botToken;
    }

    public String getOwnerID() {
        return ownerID;
    }
}