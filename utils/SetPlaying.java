package utils;


import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class SetPlaying extends ListenerAdapter {

    private static Random r = new Random();
    private static Timer timer = new Timer();
    private static String x;
    private static String[] status = {
            "YEET",
            "#FreeLBG",
            "#FreeFizzy",
            "with Jenka's eGirls",
            "with Edzy's eBoys",
            "mange la marde",
            "HAHAHAHAHAHAHA",
            "uwu"};

    public void onReady(ReadyEvent e) { //changes the "Playing" status message with 5 minute intervals.
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                x = status[r.nextInt(status.length)];
                e.getJDA().getPresence().setGame(Game.of(x));
            }
        }, 10*1000, 5*60*1000);
    }
}
