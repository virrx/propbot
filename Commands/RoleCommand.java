package Commands;

import com.jagrosh.jdautilities.commandclient.Command;
import com.jagrosh.jdautilities.commandclient.CommandEvent;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Role;
import org.apache.commons.collections4.multiset.SynchronizedMultiSet;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RoleCommand extends Command {

    public RoleCommand() {
        this.name = "role";
        this.help = "Give yourself a position";
        this.arguments = "<rank>";
        this.guildOnly = false;

    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split("\\s+");
        List<String> savedRoles = new ArrayList<>();
        Collections.addAll(savedRoles, "top", "jungle", "mid", "adc", "support", "fill", "1v1", "inhouse", "scrim");

        if (event.getArgs().toLowerCase().isEmpty() || event.getArgs().toLowerCase().equals("help")){
            EmbedBuilder eb = new EmbedBuilder();
            eb.setColor(event.getMember().getColor()) //info box
                    .setAuthor("Roles | Usage", null, "https://i.imgur.com/iKhG9Ou.jpg")
                    .addField("+rank", "unranked, bronze, silver, gold, platinum, diamond, master, challenger, spectator", false)
                    .addField("+role", "top, jungle, mid, adc, support, fill, 1v1, inhouse, scrim", false)
                    .setFooter("Prophecy Cup", null)
                    .setTimestamp(Instant.now());
            event.getTextChannel().sendMessage(eb.build()).queue();


        } else if(savedRoles.contains(args[0].toLowerCase())){

            List<Role> roles = event.getGuild().getRolesByName(args[0], true);

            if(event.getMember().getRoles().contains(roles.get(0))){
                event.getGuild().getController().removeRolesFromMember(event.getMember(), roles.get(0)).complete();
                event.reply("Removed role \"" + roles.get(0).getName() + "\" from user " + event.getMember().getAsMention());
            } else {
                event.getGuild().getController().addSingleRoleToMember(event.getMember(), roles.get(0)).queue();
                event.reply("Added role \"" + roles.get(0).getName() + "\" to user " + event.getMember().getAsMention());
            }

        } else {
            System.out.println("Unknown Role!");
            event.reply(event.getMember().getAsMention() + ", please enter a valid role!");
        }
    }
}
