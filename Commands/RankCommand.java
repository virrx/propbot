package Commands;

import com.jagrosh.jdautilities.commandclient.Command;
import com.jagrosh.jdautilities.commandclient.CommandEvent;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Viktor on 2017-09-16.
 */
public class RankCommand extends Command {

    public RankCommand() {
        this.name = "rank";
        this.help = "Give yourself a rank";
        this.arguments = "<rank>";
        this.guildOnly = false;

    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split("\\s+");
        List<String> savedRoles = new ArrayList<>();
        Collections.addAll(savedRoles, "unranked", "bronze", "silver", "gold", "platinum", "diamond", "master", "challenger", "spectator");

        if (event.getArgs().toLowerCase().isEmpty() || event.getArgs().toLowerCase().equals("help")){
            EmbedBuilder eb = new EmbedBuilder();
            eb.setColor(event.getMember().getColor()) //info box
                    .setAuthor("Roles | Usage", null, "https://i.imgur.com/iKhG9Ou.jpg")
                    .addField("+rank", "unranked, bronze, silver, gold, platinum, diamond, master, challenger, spectator", false)
                    .addField("+role", "top, jungle, mid, adc, support, fill, 1v1, inhouse, scrim", false)
                    .setFooter("Prophecy Cup", null)
                    .setTimestamp(Instant.now());
            event.getTextChannel().sendMessage(eb.build()).queue();


        } else if(savedRoles.contains(args[0].toLowerCase())){

            List<Role> unfilteredList = event.getGuild().getRoles();;

            List<Role> filteredList = unfilteredList
                    .stream()
                    .filter(e -> savedRoles.contains(e.getName().toLowerCase()))
                    .collect(Collectors.toList());
            List<Role> roles = event.getGuild().getRolesByName(args[0], true);

            boolean filter = event.getMember().getRoles().stream().anyMatch(filteredList::contains);

            if(filter){
                event.getGuild().getController().removeRolesFromMember(event.getMember(), filteredList).complete();
            }
            event.getGuild().getController().addSingleRoleToMember(event.getMember(), roles.get(0)).queue();
            event.reply("Added rank \"" + roles.get(0).getName() + "\" to user " + event.getMember().getAsMention());
        } else {
            System.out.println("Unknown Role!");
            event.reply(event.getMember().getAsMention() + ", please enter a valid rank!");
        }
    }
}

